"use strict"

function createNewUser() {
  let newUser = {};

  let firstName = prompt("Введіть ім'я: ");
  let lastName = prompt("Введіть прізвище: ");

  newUser.firstName = firstName;
  newUser.lastName = lastName;

  newUser.getLogin = function() {
    return (this.firstName.charAt(0) + this.lastName).toLowerCase();
  };

  newUser.setFirstName = function(newFirstName) {
    this.firstName = newFirstName;
  };

  newUser.setLastName = function(newLastName) {
    this.lastName = newLastName;
  };

  return newUser;
}

let user = createNewUser();

console.log(user.getLogin());
